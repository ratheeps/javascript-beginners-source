var myNumber = 15;
var a = myNumber.toString(16); // returns 80
document.write(a);
var b = myNumber.toString(8); // returns 200
document.write('<br>');
document.write(b);
var c = myNumber.toString(2); // returns 10000000
document.write('<br>');
document.write(c);

//NaN

var nu = 10;
var num = 'Apple';
document.write('<br>');
document.write( isNaN(nu) );
document.write('<br>');
document.write( isNaN(num) );
document.write('<br>');

// Global methods
var x = 9.656;
var ac = x.toExponential(2);
document.write(ac);
document.write('<br>');

var y = 9.656;
var m = x.toFixed(4);
document.write(m);
document.write('<br>');

var z = 9.656;
var k = x.toPrecision(3);
document.write(k);
document.write('<br>');

var bv = true;
var cv = Number(bv);
document.write(cv);
document.write('<br>');

var cd = "1000 10 10";
var cbn = parseInt(cd);
document.write(cbn);
document.write('<br>');