var a = 5;
var b = 1;
var outPut = null;
outPut = a & b;
document.write('(a & b) = ' + outPut + '<br/>');

outPut = a | b;
document.write('(a | b) = ' + outPut + '<br/>');

outPut = ~a;
document.write('(~a) = ' + outPut + '<br/>');

outPut = a ^ b;
document.write('(a ^ b) = ' + outPut + '<br/>');

outPut = a << b;
document.write('(a << b) = ' + outPut + '<br/>');

outPut = a >> b;
document.write('(a >> b) = ' + outPut + '<br/>');