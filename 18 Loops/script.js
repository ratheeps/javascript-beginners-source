for (var i = 0; i < 10; i++){
    if (i == 5){
        continue;
    }
    document.write(i);
    document.write('<br>');
}

var a = 0;
while (a < 10){
    if (a == 5){
        break;
    }
    document.write(a);
    document.write('<br>');
    a++
}

var b = 0;
do{
    document.write(b);
    document.write('<br>');
    b++
}while (b < 100);

var c = [1, 3, 4, 5, 6, 7, 8];
for (var d = 0; d < c.length; d++){
    document.write(c[d]);
    document.write('<br>');
}

var e = {a:1, b:2, c:3, d:4};
for (v in e){
    document.write(e[v]);
    document.write('<br>');
}


top:
for(var l = 0; l<10; l++){
    for(var g = 0; g<10; g++){
        if (g == 5){
            break top;
        }
        document.write(g);
        document.write('<br>');
    }
    document.write('-----');
    document.write('<br>');
}