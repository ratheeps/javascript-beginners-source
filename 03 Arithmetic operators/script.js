// Arithmetic operatorsvar
var c = 10 + 20;
document.write( 'Addition 10 + 20 = ' + c + '<br/>');

var d = 20 - 10;
document.write( 'Subtraction 20 - 10 = ' + d + '<br/>');

var e = 20 * 10;
document.write( 'Multiplication 20 * 10 = ' + e + '<br/>');

var f = 20 / 10;
document.write( 'Division 20 / 10 = ' + f + '<br/>');

var g = 11 % 3;
document.write( 'Modulus 11 % 3 = ' + g + '<br/>');

var a = 10;
var h = a++;
document.write( 'Increment (a = 10;  h = a++ ;) <br/> h =' + h + '<br/>');
document.write( 'a =' + a + '<br/>');

var b = 10;
var i = ++b;
document.write( 'Increment (b = 10;  i = ++b ;) <br/> i =' + i + '<br/>');
document.write( 'b =' + b + '<br/>');


var x = 10;
var y = x--;
document.write( 'Decrement (x = 10;  y = x-- ;) <br/> y =' + y + '<br/>');
document.write( 'x =' + x + '<br/>');

var k = 10;
var l = --k;
document.write( 'Increment (k = 10;  l = --k ;) <br/> l =' + l + '<br/>');
document.write( 'k =' + k + '<br/>');