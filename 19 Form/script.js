function validateForm(){
    var validation = true;
    var fname = document.forms['register']['fname'].value;
    var lname = document.forms['register']['lname'].value;
    var email = document.forms['register']['email'].value;
    if (fname === '' || fname === null)
    {
        printError('fname-error', 'Please enter your first name');
        validation = false;
    }

    if (lname === '' || lname === null)
    {
        printError('lname-error', 'Please enter your last name');
        validation = false;
    }

    if (email === '' || email === null)
    {
        printError('email-error', 'Please enter your email');
        validation = false;
    }else{
        var atPos = email.indexOf('@');
        var dotPos = email.lastIndexOf('.');
        if( atPos < 2 || (dotPos - atPos) <= 2 || (email.length - dotPos) <= 2){
            printError('email-error', 'Please enter valid email');
            validation = false;
        }

    }

    return validation;
}

function printError(id, msg) {
    document.getElementById(id).innerHTML= msg;
}