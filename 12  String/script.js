function fun() {
    var myString = "Apple";
    var cAt = myString.charAt(3);
    document.write('charAt(3) = ' + cAt + '<br>');

    var CC = myString.concat('Orange');
    document.write('concat("Orange") = ' + CC + '<br>');

    var iof = myString.indexOf('p');
    document.write('indexOf("p") = ' + iof + '<br>');

    var liof = myString.lastIndexOf('p');
    document.write('lastIndexOf("p") = ' + liof + '<br>');

    var l = myString.length;
    document.write('length = ' + l + '<br>');

    var str = "Please locate where 'locate' occurs!";
    var pos = str.search("locate");
    document.write('search("locate") = ' + pos + '<br>');

    var sl = myString.slice(2);
    document.write('slice(3) = ' + sl + '<br>');

    var sl2 = myString.slice(1, 3);
    document.write('slice(1, 3) = ' + sl2 + '<br>');

    str = "Please visit Microsoft!";
    var n = str.replace(/Microsoft/g,"W3Schools");
    document.write('replace(/Microsoft/g,"W3Schools") = ' + n + '<br>');

    var up = myString.toLocaleUpperCase();
    document.write('toLocaleUpperCase() = ' + up + '<br>');

    var txt = "a,b,c,d,e"; // String
    var sp = txt.split(","); // =>  ['a', 'bb', 'c', 'd', 'e']
    document.write(sp[1]);
}
