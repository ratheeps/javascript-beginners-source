var student = {
    'first_name' : 'Raja',
    'last_name' : 'Vemal',
    'age' : 10
};

document.write('First Name: ' + student.first_name);
document.write('<br/>');
document.write('Last Name: ' + student['last_name']);
document.write('<br/>');
document.write('Age: ' + student.age);