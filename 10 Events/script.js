function onChange(value) {
    printText(value);
}

function onClick() {
    printText('clicked');
}

function onMouseOver() {
    printText('Over the mouse');
}

function onMouseOut() {
    printText('Mouse out');
}

function onKeyDown() {
    printText('Key pushed');
}


function onLoad() {
    printText('Body loaded');
}

function printText(text) {
    document.getElementById('text').innerHTML = text;
}