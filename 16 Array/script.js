var ar1 = new Array(10, 11, 13, 15);
var ar2 = [10, 11, 13, 'Apple'];

document.write(ar1[2]);
document.write('<br>');
ar2[3] = 'Orange';
document.write(ar2[3]);

document.write('<br>');
document.write(ar2.toString());

document.write('<br>');
document.write(ar2.join('/'));

document.write('<br>');
ar2.pop();
document.write(ar2.toString());

document.write('<br>');
ar1.push('ABC');
document.write(ar1.toString());

document.write('<br>');
ar1.shift();
document.write(ar1.toString());


document.write('<br>');
ar1.unshift('Apple 1');
document.write(ar1.toString());

document.write('<br>');
ar1.splice(2,2, "D", "M", 'N');
document.write(ar1.toString());

document.write('<br>');
ar1.sort();
document.write(ar1.toString());

document.write('<br>');
ar1.reverse();
document.write(ar1.toString());

document.write('<br>');
var ar3 = ar1.concat(ar2);
document.write(ar3.toString());

document.write('<br>');
var ar4 = ar3.slice(1, 5);
document.write(ar4.toString());